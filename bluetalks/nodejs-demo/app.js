const restify = require('restify')

const server = restify.createServer({
  name: 'my-restify-app',
  version: '1.0.0'
})

// para nao retornar um inteiro e dar a ideia do front forçar uma sequencia
// utilizamos um uuid - https://www.uuidgenerator.net/
const produtos = [
  { id: '0dcc0416-31d3-43b6-86c4-15af84e9d928', name: 'IphoneR', tipo: 'mobile', preco: 1001.25 },
  { id: '2a802b6c-b3d8-46d7-9e9b-0dce7e59b806', name: 'Samsung Galax', tipo: 'mobile', preco: 800.15 }
]
// chamada no browser localhost:3000 com o metodo hello
// 3 parametros: objeto de requisicao, objeto de resposta, objeto de controle de chamada de outra funcao next 
server.get('/produtos', (req, resp, next)=>{
  
  //resp.writeHead(200,{'Content-Type': 'text/html'})
  resp.json(produtos)
   //resp.write('teste fabrizio')
  //resp.end()
  return next()
})

// a chamada agora é passando um parametro no metodo GET. 
// a funcao utilizada é a filter, filtra o parametro recebido do path e compara com o elemento do array
// a funcao resp do restify prove o parametro status
server.get('/produtos/:id', (req, resp, next)=>{
  const filtro = produtos.filter(produto => produto.id === req.params.id)
  if (filtro.length) {
    resp.json(filtro[0])
  } else {
    resp.status(404)
    resp.json({status: 404, message: 'produto nao encontrado'})
  }
  return next()
})

// servidor escutando na porta informado. rodando......
server.listen(3001, ()=>{
  console.log('servidor escutando na porta 3001')
})

