# Bluetalks - executando imagens Docker

### O Dockerfile é usado para criar Builds reproduzíveis para o seu aplicativo. Um fluxo de trabalho comum é fazer com que sua automação de CI/CD execute o comando "docker image build" como parte de seu processo de criação. Após a criação das imagens, elas serão enviadas para um central registry, onde poderão ser acessadas por todos os ambientes que precisam executar instâncias desse aplicativo. A imagem personalizada para o registro público do Docker, eh o Docker Hub, onde pode ser consumido por outros desenvolvedores e operadores.

## O Dockerfile contem as instrucoes necessarias linha por linha para criar uma imagem do Docker. Abaixo a explicacao das TAG'S dentro do Dockerfile
- FROM: Indica a imagem de origem (deve ser a primeira linha).
- MAINTAINER: Descricao do responsavel pela imagem.
- RUN: Executa um determinado comando dentro da imagem (criará um layer a cada RUN)
- CMD: Proposito de executar um comando final na imagem
- ENTRYPOINT: Binario que será executado após a incialização do container.
- EXPOSE: A porta que a imagem possui para uma determinada porta TCP / UDP (PortaHost:PortaContainer)
- ADD: Adiciona um arquivo empacotado (tar) no destino, link (url) ou arquivo.
- ENV: Informa variaveis de ambiente ao container.
- VOLUME: Permite a utilização de um ponto de montagem.
- USER: Determina qual o usuario será utilizado na imagem. (default root)

Tips: O arquivo deve seguir o padrao de iniciar com a letra "D" em maiusculo.

### definicao para criacao da imagem de uma aplicacao
```Dockerfile
FROM ibmcom/iib-mq-server
MAINTAINER Fabrizio Sales fabrizio.rodrigues@ibm.com

RUN mkdir /tmp/BARs
ADD APP_CLIENTEproject.generated.bar /tmp/BARs/
ADD APP_CLIENTE_JSONproject.generated.bar /tmp/BARs/

USER root
RUN chown iibuser:mqbrkrs /tmp/BARs/*
RUN chmod +x /tmp/BARs/APP_CLIENTEproject.generated.bar
RUN chmod +x /tmp/BARs/APP_CLIENTE_JSONproject.generated.bar

#COPY *.sh /usr/local/bin/

USER iibuser
#Set entrypoint to run management script
#ENTRYPOINT ["mqsichangeproperties NODE3 -e IS03 -o HTTPConnector -n corsEnabled -v true"]
```
### para criar a imagem a partir do Dockerfile, informe o nome para a imagem seguido ou nao da versao, e o <caminho> do arquivo, no caso pode ser o corrente local "." ponto:
```sh
$docker build -t iib_teste:1.0 .
```

### acessando os containers. Suprimindo o parametro "-a", voce visualiza apenas os containers em execucao. fornecendo --help após o comando, voce tem acesso a ajuda
```sh
$docker ps -a
CONTAINER ID        IMAGE                   COMMAND                  CREATED             STATUS                      PORTS                                                                       NAMES
41bd9cfe4c92        iib_iib-op              "iib_manage.sh"          22 hours ago        Exited (255) 15 hours ago   0.0.0.0:32414->1414/tcp, 0.0.0.0:34414->4414/tcp, 0.0.0.0:37800->7800/tcp   iib_iib-op_1
```
### Para reiniciar, parar e iniciar o container faca o stop/start/restart + container
```sh

$docker stop/start/restart 41bd9cfe4c92
```

### visualizando os logs do container. Informando apenas os 3 primeiros bytes, voce tem acesso ao log
```sh
$docker logs 41b

```


### para vizualizar as imagens
```sh
$docker images
REPOSITORY              TAG                 IMAGE ID            CREATED             SIZE
swaggerapi/swagger-ui   latest              0c6b605e07c7        4 days ago          67.1MB
ibmcom/datapower        latest              0028dae0fa2b        3 weeks ago         882MB
node                    8-alpine            cf491d6b25e2        6 months ago        66.3MB
ibmcom/iib-mq-server    latest              6df86add090c        16 months ago       1.77GB
sg248351/eventqm        latest              d84147d8147d        2 years ago         879MB
```
### remover o container do docker
```sh

$docker rm 83593ae50750
```
 
### remover a imagem 
```sh

$docker rmi fd4716277042
```
### Extrair imagem do hub docker
```sh

$docker pull {nome da imagem}
```

### executar comandos na imagem
```sh
$docker exec -it container bash
```
### visualizar as configuracoes da imagem 

```sh
$docker inspect sg248351/iib-op
```

### para limpar as imagens iniciadas e paradas
```sh
$docker system prune
```
 
### visualizar memoria, container e cpu usada
```sh
$docker stats
```

### criando infra em cluster para os containers.
O Swarm trabalha na estrutura Manager / Worker, onde a figura do manager tem a funcao de controlar e orquestrar os servidores (Workers)
```sh
$docker swarm init
Swarm initialized: current node (b007227ii891z46kjhbnwaruc) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-4snakxzsiw4y6lv3poyprpnzdidv2woy79kiph7mwxnm9jxylx-9cq0xzn9gohvbb55p2n5wjczf 192.168.65.3:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```
### listando os nodes
```sh
$ docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS
7x9s8baa79l29zdsx95i1tfjp     node3               Ready               Active
x223z25t7y7o4np3uq45d49br     node2               Ready               Active
zdqbsoxa6x1bubg3jyjdmrnrn *   node1               Ready               Active              Leader
```

### Criando um serviço com replicas
```sh
$docker service create --name iib-op --replicas 5 -p 47800:7800  iib_iib-op
```

### Listando os "processos" do serviço
```sh
$docker service ps iib-op
```

:+1: